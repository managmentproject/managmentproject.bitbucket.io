const BASE_URL = 'https://randomuser.me/api/?results=100'
// import * as sort from 'https://cdnjs.cloudflare.com/ajax/libs/tablesort/5.0.2/tablesort.min.js';

class Person{
    constructor(element,index){

        this.gender = element.gender;
        this.name = {
            firstName : element.name.first,
            lastName : element.name.last,
        };
        this.date = {
            age: element.dob.age,
            dateOfBirth: this.dateToString(element.dob.date),
        };
        this.email = element.email;
        this.username = element.login.username;
        this.location = {
            country : element.location.country,
            city: element.location.city,
            nameStreet : element.location.street.name,
            numStreetHome : element.location.street.number,
        };
        this.registration = {
            age : element.registered.age,
            date : this.dateToString(element.registered.date),
        };
        this.telNum = element.phone;
        this.id = index;
    }

    dateToString(date) {
        return date.split('T')[0];
    }
}


let paginationPersons = [];

// preloader

let mask = document.querySelector('.mask');

// function pagination

let countOfItemInPage = 15;
let active;
let itemsLi = [];
let activePagination;

function showCountItem(){
    let ul = document.querySelector('#pagination');
    ul.innerHTML = '';
    for(let i = 0; i < Math.ceil(persons.length / countOfItemInPage); i++){
        ul.innerHTML += `<li>${i+1}</li>`;
    }

    addListener();
}


function addListener(){
    let items = document.querySelectorAll('#pagination li');
    itemsLi = items;
    for(let item of itemsLi){
        item.addEventListener('click', function(){
            pagination(this);
        })
    }
}

function pagination(item){
    if(active){
        active.classList.remove('active');
    }
    active = item;

    item.classList.add('active');

    let pageNum = +item.innerHTML;

    let start = (pageNum - 1) * countOfItemInPage;
    let end = start + countOfItemInPage;

    paginationPersons = persons.slice(start,end);

    fillTable(paginationPersons);
}

// end of pagination

function displayLoading(){
    mask.style.display = 'flex';
}

function hideLoading(){
    mask.style.display = 'none';
}

function fillTable(persons) {
    const output = persons.map((element) => `
    <tr class='user' data-key=${element.id}>
        <td>${element.gender === 'male' ? 'Мужской' : 'Женский'}</td>
        <td>${element.name.firstName}</td>
        <td>${element.name.lastName}</td>
        <td>${element.date.dateOfBirth}</td>
        <td>${element.email}</td>
        <td>${element.username}</td>
    </tr>`).join('');
    startcont.style.display = 'none';
    document.querySelector('.container_nav').style.display = 'flex';
    document.querySelector('#output').style.display = 'flex';
    document.querySelector('.tbody').innerHTML = output;
    hideLoading();
}

let table = document.querySelector('.table');
let startcont = document.querySelector('.btn-container');
let res = JSON.parse(localStorage.getItem('persons'));
let output = '';
let persons = new Array()   
let popup = document.querySelector('.popup-bg');
// startbtn.addEventListener('click', getData);

if(!!res && res.length != 0){
    displayLoading();
    persons = res;
    showCountItem();
    pagination(itemsLi[0]);;
}

function getData(){
    displayLoading();
    fetch(BASE_URL)
        .then(res => res.json())
        .then(data =>{
            let author = data.results;
            let i = 0;
            author.forEach((element, ind) => {
                const person = new Person(element, ind);
                persons.push(person);
            });
            showCountItem();
            pagination(itemsLi[0]);
            localStorage.setItem('persons',JSON.stringify(persons));
            hideLoading();
        });
}

// functions change user data

document.querySelector('#popup-exit').addEventListener('click',function(){
    popup.style.display = 'none';
});


document.querySelector('.tbody').addEventListener('click', changeUser)


let newTarget;
let index;

function changeUser(c){
    let ind = c.target.parentNode.dataset.key;
    showPopup(persons[ind]);
    index = ind;
}

function showPopup(elem){
    inputs.forEach((input) =>{
        input.style.border = '1px solid black';
    });
    popup.style.display = 'flex';
    if(elem.gender == 'male'){
        document.querySelector('#female').removeAttribute('selected');
        document.querySelector('#male').setAttribute('selected', true);
    }
    else
    {
        document.querySelector('#male').removeAttribute('selected');
        document.querySelector('#female').setAttribute('selected',true);
    }
    document.querySelector('#firstName').value = elem.name.firstName;
    document.querySelector('#firstName').placeholder = elem.name.firstName;
    document.querySelector('#lastName').value = elem.name.lastName;
    document.querySelector('#lastName').placeholder = elem.name.lastName;
    document.querySelector('#age').value = elem.date.age;
    document.querySelector('#age').placeholder = elem.date.age;
    document.querySelector('#dateOfBirth').value = elem.date.dateOfBirth;
    document.querySelector('#dateOfBirth').placeholder = elem.date.dateOfBirth;
    document.querySelector('#email').value = elem.email;
    document.querySelector('#email').placeholder = elem.email;
    document.querySelector('#username').value = elem.username;
    document.querySelector('#username').placeholder = elem.username;
    document.querySelector('#country').value = elem.location.country;
    document.querySelector('#country').placeholder = elem.location.country;
    document.querySelector('#city').value = elem.location.city;
    document.querySelector('#city').placeholder = elem.location.city;
    document.querySelector('#nameStreet').value = elem.location.nameStreet;
    document.querySelector('#nameStreet').placeholder = elem.location.nameStreet;
    document.querySelector('#numStreetHome').value = elem.location.numStreetHome;
    document.querySelector('#numStreetHome').placeholder = elem.location.numStreetHome;
    document.querySelector('#ageReg').innerHTML = `Возраст во время регистрации: ${elem.registration.age}`;
    document.querySelector('#dateReg').innerHTML = `Дата регистрации: ${elem.registration.date}`;
    document.querySelector('#telNum').value = elem.telNum;
    document.querySelector('#telNum').placeholder = elem.telNum;
    let im = new Inputmask('+375(99)999-99-99');
    im.mask(document.querySelector('#telNum'));
}


function closePopupErr(){
    document.querySelector('.popup_err-bg').style.display = 'none';
}

document.querySelector('#popup-save').addEventListener('click', saveItems)
document.querySelector('#popup-delete').addEventListener('click', deleteUser);


let modalWin = document.querySelector('.modalWin-bg');

const inputs = Array.from(document.querySelectorAll('.popup-output input, .popup-output select'));

function saveItems(){
    const values = inputs.reduce((acc, input) => ({
        ...acc,
        [input.dataset.item]: input.value,
    }), {});
    const validInputsCount = inputs.map(input => validate(input)).filter(inputStatus => inputStatus).length;
    if(Object.keys(values).length === validInputsCount){
        displayLoading();
        const Data = prepareData(values);
        persons[index] = {
            ...persons[index],
            ...Data,
        };
        localStorage.setItem('persons',JSON.stringify(persons));
        popup.style.display = 'none';
        pagination(itemsLi[active.innerHTML - 1]);
    }

    function prepareData(values){
        return {
            gender: values.gender == 'Мужской' ? 'male' : 'female',
            name: {
                firstName: values.firstName,
                lastName: values.lastName,
            },
            date: {
                age: values.age,
                dateOfBirth: values.dateOfBirth,
            },
            email: values.email,
            username: values.username,
            location: {
                country: values.country,
                city: values.city,
                nameStreet: values.nameStreet,
                numStreetHome: values.numStreetHome,
            },
            telNum: values.telNum,
        };
    }


    function validate(input){
        const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let result;
        switch(input.dataset.item){
            case 'gender':
            case 'firstName':
            case 'lastName':
            case 'dateOfBirth':
            case 'username':
            case 'country':
            case 'city':
            case 'nameStreet':
            case 'numStreetHome':
                result = input.value.trim().length > 0;
                break;
            case 'telNum':
                const telPattern = /^\+375 \((17|29|33|44)\) [0-9]{3}-[0-9]{2}-[0-9]{2}$/;
                console.log(input.value)
                result = telPattern.test(input.value) == true;
                break;
            case 'age':
                result = input.value > 0 && input.value < 110;
                break;
            case 'email':
                result = input.value.length > 0 && pattern.test(input.value) == true;
                break;
            default:
                result = false;
                break;
        }
        if(!result){
            input.value = '';
            input.placeholder = 'Некорректное значение!';
            input.style.border = '1px solid red';
        }
        return result;
    }
}

function yesDeleteUser(){
    displayLoading();
    persons.splice(index,1);
    persons.forEach((elem, ind) =>{
        elem.id = ind;
    });
    localStorage.setItem('persons',JSON.stringify(persons));
    if(persons.length != 0){
        showCountItem();
        pagination(itemsLi[active.innerHTML - 1]);
    }
    else{
        showCountItem();
        startcont.style.display = 'flex';
        document.querySelector('#output').style.display = 'none';
    }
    modalWin.style.display = 'none';
    hideLoading();
}

function noDeleteUser(){
    modalWin.style.display = 'none';
}

function deleteUser(){
    popup.style.display = 'none';
    modalWin.style.display = 'flex';
}


// function search

let search = document.querySelector('#searchinput');

search.addEventListener('keydown', function(e){
    if(e.key === 'Enter'){
        if(this.value.trim() != ''){
            e.preventDefault();
            let res = new Array();
            res = persons.filter((el) => 
                el.name.firstName.toLowerCase().includes(this.value.toLowerCase()) 
                || el.name.lastName.toLowerCase().includes(this.value.toLowerCase()) 
                || el.email.toLowerCase().includes(this.value.toLowerCase()) 
                || el.username.toLowerCase().includes(this.value.toLowerCase())
            );
            fillTable(res);
        }
        else{
            pagination(itemsLi[active.innerHTML - 1]);
        }
    }
});

let searchbtn = document.querySelector('#searchbtn');

searchbtn.addEventListener('click', function(){
    let searchIn = search.value.trim();
    if(searchIn.length == 0){
        pagination(itemsLi[active.innerHTML - 1]);
    }
    else{
        let res = new Array();
        res = persons.filter((el) => el.name.firstName.toLowerCase().includes(searchIn.toLowerCase()) 
        || el.name.lastName.toLowerCase().includes(searchIn.toLowerCase()) 
        || el.email.toLowerCase().includes(searchIn.toLowerCase()) 
        || el.username.toLowerCase().includes(searchIn.toLowerCase()));
        fillTable(res);
    }
});

document.querySelector('#clearbtn').addEventListener('click',function(){
    search.value = '';
    pagination(itemsLi[active.innerHTML - 1]);
});


